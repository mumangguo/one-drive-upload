package com.mmg.onedriveupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneDriveUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(OneDriveUploadApplication.class, args);
    }

}
