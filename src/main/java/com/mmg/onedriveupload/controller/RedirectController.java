package com.mmg.onedriveupload.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedirectController {

    /**
     * 回调地址
     * http://localhost:8080/redirectUri?code=M.C105_BL2.2.127d6530-7077-3bcd-081e-49be3abc3b45
     *
     * @param code
     * @return
     */
    @GetMapping("/redirectUri")
    public String redirectUri(String code) {
        return code;
    }
}
