package com.mmg.onedriveupload.controller;

import com.alibaba.fastjson.JSONObject;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * 文件上传到OneDrive并将文件信息存储到Excel文件中
 */
@Controller
public class FileSyncController {
    private static final Logger logger = LoggerFactory.getLogger(FileSyncController.class);
    private static final String ONE_DRIVE_BASE_URL = "https://graph.microsoft.com/v1.0/me/drive/root:/uploadfile/";

    @Value("${onedrive.access-token}")
    private String ACCESS_TOKEN;


    @PostMapping("/upload")
    public void uploadFileToDrive(@RequestParam("file") MultipartFile file, HttpServletResponse httpServletResponse) throws IOException {
        if (file.isEmpty()) {
            throw new RuntimeException("文件为空！");
        }

        String fileName = file.getOriginalFilename();
        String oneDriveUploadUrl = ONE_DRIVE_BASE_URL + fileName + ":/content";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setBearerAuth(ACCESS_TOKEN);

        HttpEntity<byte[]> requestEntity;
        try {
            byte[] fileBytes = file.getBytes();
            requestEntity = new HttpEntity<>(fileBytes, headers);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(oneDriveUploadUrl, HttpMethod.PUT, requestEntity, String.class);

        if (response.getStatusCode() == HttpStatus.CREATED) {
            //解析返回的JSON字符串，获取文件路径
            String downloadUrl = JSONObject.parseObject(response.getBody()).getString("@microsoft.graph.downloadUrl");
            storeFileInfoToExcel(fileName, downloadUrl);
            logger.info("文件上传成功，OneDrive 文件路径：" + downloadUrl);
            httpServletResponse.setCharacterEncoding("utf-8");
            httpServletResponse.setContentType("text/html;charset=utf-8");
            PrintWriter out = httpServletResponse.getWriter();
            out.print("<script> alert('" + fileName + "已上传成功！');window.location.href='file_info.xlsx';</script>");
        } else {
            throw new RuntimeException("文件上传失败！");
        }
    }

    /**
     * 将文件信息存储到Excel文件中
     *
     * @param filename 文件名称
     * @param filepath 文件路径
     */
    private void storeFileInfoToExcel(String filename, String filepath) {
        try {
            File file = new File(ResourceUtils.getURL("classpath:").getPath() + "/static/file_info.xlsx");

            XSSFWorkbook excel;
            XSSFSheet sheet;
            FileOutputStream out;

            // 如果文件存在，则读取已有数据
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                excel = new XSSFWorkbook(fis);
                sheet = excel.getSheetAt(0);
                fis.close();
            } else {
                // 如果文件不存在，则创建一个新的工作簿和工作表
                excel = new XSSFWorkbook();
                sheet = excel.createSheet("file_info");
                // 创建表头
                XSSFRow headerRow = sheet.createRow(0);
                headerRow.createCell(0).setCellValue("文件名称");
                headerRow.createCell(1).setCellValue("文件路径");
            }

            // 获取下一个行号
            int rowNum = sheet.getLastRowNum() + 1;
            // 创建新行
            XSSFRow newRow = sheet.createRow(rowNum);
            newRow.createCell(0).setCellValue(filename);
            newRow.createCell(1).setCellValue(filepath);

            // 将数据写入到文件
            out = new FileOutputStream(file);
            excel.write(out);

            // 关闭资源
            out.close();
            excel.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

